﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(CharacterController))]
[AddComponentMenu("Control Script/FPS Input")]
public class FPSInput : MonoBehaviour {
    public float speed = 6.0f;
    public float gravity = -9.8f;
    public float jumpSpeed = 10.0f;
    private CharacterController _charController;
    private float deltaY = 0.0f;
	// Use this for initialization
	void Start () {
        _charController = GetComponent<CharacterController>();
        
	}
	
	// Update is called once per frame
	void Update () {
       
        float deltaX = Input.GetAxis("Horizontal") * speed;
        float deltaZ = Input.GetAxis("Vertical") * speed;

        if (Input.GetButton("Jump"))
            deltaY = jumpSpeed;

        Vector3 movement = new Vector3(deltaX, 0, deltaZ);
   
        movement = Vector3.ClampMagnitude(movement, speed);
        movement.y = deltaY + gravity;
        deltaY = 0.0f;
        movement *= Time.deltaTime;
        movement = transform.TransformDirection(movement);
        _charController.Move(movement);
        deltaY = 0.0f;
    }
}
